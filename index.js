// console.log("Hey Jude")

// EXPONENT OPERATOR

	// Before ES6
		const firstNum = 8 ** 2;
		console.log(firstNum);
	// ES6
		const secondNum = Math.pow(8, 2);
		console.log(secondNum);

// TEMPLATE LITERALS - allows us to write strings w/o using concatenation operator (+)

		let name = "John";
			let message = 'Hello ' + name + '! Welcome to programming.'
			console.log(message)

		// using backticks(``);

			message = `Hello 
			${name}! 
			Welcome 
			to 
			programming.`
			console.log(message);

		// Template literals allow us to write strings with embedded JS.

			const interestRate = 0.1;
			const principal = 1000;
			console.log(`The interest on your savings is: ${interestRate*principal}.`);

// 	ARRAY DESTRUCTURING 
/*
	- allows us to unpack elements in arrays into distinct variables. It allows us to name array elements with variables instead of using index numbers.
	-syntax: let/const [varA, varB, ...] = arrayName
*/

		const fullName = ["Juan", "Dela", "Cruz"];
		// Before array destructuring
			console.log(fullName[0]);
			console.log(fullName[1]);
			console.log(fullName[2]);
			console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you`);

		// Array destructuring
			const[firstName, middleName, lastName] = fullName;
			console.log(firstName);
			console.log(middleName);
			console.log(lastName);

			console.log(fullName)
			console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`)

// OBJECT DESTRUCTURING	
/*
	-allows us to unpack properties of objects into distinct variable

	syntax:
			let/const { propertyA, propertyB} = objectName
*/		

			const person = {
				givenName: "Jane",
				maidenName: "Dela",
				familyName: "Cruz"
			}

			// Before the object 
				console.log(person.givenName);
				console.log(person["maidenName"])

				let {givenName, maidenName, familyName} = person;
				console.log(givenName);
				console.log(maidenName);
				console.log(familyName);

			// function getFullName({givenName, maidenName, familyName}){
			// 	console.log(`${givenName ${maidenName} ${familyName}`)
			// }

// ARROW FUNCTIONS

		const hello = () => {
			console.log("Hello World");
		}

		//function expression
		// const hello = function(){
		// 	console.log("Hello World");
		// }

		hello();

		// before arrow function and template literals
		function printFullName(firstName, middleInitial, lastName){
			console.log(firstName + ' ' + middleInitial + ' ' + lastName)
		}

		printFullName("Chris", "O.", "Mortel")

		// After
		let fName = (firstName, middleInitial, lastName) => {
			console.log(`${firstName} ${middleInitial} ${lastName}`)
		}
		fName("Chris", "O.", "Mortel")


		// Arrow function with loops

		const student = ["John", "Jane", "Judy"];

		// before arrow function
			// for every element of student array, log "studentName is a student"

		function iterate(student){
			console.log(student + " is a student")
		}
		student.forEach(iterate)


		// after arrow function
		let students = (student) => {
			console.log(`${student} is a student`)
		}
		student.forEach(students)


		// alternative
		student.forEach((student)=>{
			console.log(`${student} is a student!`)
		})


// IMPLICIT RETURN STATEMENT
/*
	- omit return statement
*/

	const add = (x,y) => {
		console.log(x+y);
	}

	let sum = add(23, 25);
	console.log("Contains sum varibal")
	console.log(sum);

	const subtract = (x,y) => x-y;

	subtract(10, 5);
	let difference = subtract(10,5);
	console.log(difference);

// DEFAULT FUNCTION ARGUMENT VALUE
/*
	provide a default argument value if none is provided when the function is invoked.	
*/

	const greet = (name = "User") => {
		return `Good morning, ${name}`;
	}
	console.log(greet());


// CLASS-BASED OBJECTS BLUEPRINTS
	// creating class
	/*
		syntax:
		class className{
			constructor (objectValueA, objectValueB, ...){
			this.objectPropertyA = objectValueA;
			this.objectPropertyB = objectValueB;
			}
		}
	*/

	class Car{
		constructor(brand, name, year){
			this.carBrand = brand;
			this.carName = name;
			this.carYear = year;
		}
	}
		let car = new Car("Toyota", "Hilux-pickup", "2015")
		console.log(car);

		car.carBrand = "Nissan";
		console.log(car)